package com.example.finalhomework.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

import com.example.finalhomework.databinding.ListItemBinding
import kotlinx.coroutines.CoroutineScope

class RVAdapter(val context: FragmentHomework, private val cell: ArrayList<Content>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = ListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CellViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val itemViewHolder = holder as CellViewHolder
        holder.onBind(position)

    }

    override fun getItemCount(): Int {
        return cell.size
    }
    inner class CellViewHolder(var binding: ListItemBinding) : RecyclerView.ViewHolder(binding.root){
        lateinit var curItem:Content
        fun onBind(position: Int){
        curItem = cell[position]
        binding.tvDescriptionKA.text = curItem.descriptionKA.toString()
            binding.tvTitleKA.text = curItem.titleKA.toString()
            binding.tvPublishDate.text = curItem.publish_date.toString()
//            binding.tvCover.setImageResource()
            Glide.with(context).load(curItem.cover).
            into(binding.tvCover)
//            Picasso.get().load(curItem.cover).into(binding.tvCover)
        }
    }
}