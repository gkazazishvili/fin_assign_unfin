package com.example.finalhomework.ui.main

import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.finalhomework.databinding.FragmentHomeworkBinding
import com.example.finalhomework.ui.main.Constants.Companion.BASE_URL
import kotlinx.coroutines.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class FragmentHomework() : BaseFragment<FragmentHomeworkBinding, PostsViewModel>(FragmentHomeworkBinding::inflate) {
    lateinit var adapter: RVAdapter

    override fun getViewModel() = PostsViewModel::class.java

    override fun start() {
        setupRecycler()
        fetchData()
    }

    private fun setupRecycler(){
        val layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.layoutManager = layoutManager
    }
    private fun fetchData() {

        val itemsArray: ArrayList<Content> = ArrayList()
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(SimpleApi::class.java)


        CoroutineScope(Dispatchers.IO).launch {
            val response = service.getPost()
            withContext(Dispatchers.Main) {
                if (response?.isSuccessful == true) {

                    val items = response.body()?.content
                    if (items != null) {
                        for (i in 0 until items.count()) {
                            val descriptionKA = items[i].descriptionKA ?: "N/A"
                            Log.d("Response", descriptionKA)
                        val model:Content = Content(
                            items[i].category,
                            items[i].cover,
                            items[i].created_at,
                            items[i].descriptionEN,
                            items[i].descriptionKA,
                            items[i].descriptionRU,
                            items[i].id,
                            items[i].isLast,
                            items[i].publish_date,
                            items[i].published,
                            items[i].titleEN,
                            items[i].titleKA,
                            items[i].titleRU,
                            items[i].updated_at
                        )
                            itemsArray.add(model)
                            adapter = RVAdapter(this@FragmentHomework, itemsArray)
                            adapter.notifyDataSetChanged()


                        }
                    }

                    binding.recyclerView.adapter = adapter
                } else {

                    Log.e("RETROFIT_ERROR", response?.code().toString())

                }

            }

        }







    }
}







