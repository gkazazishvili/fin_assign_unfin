package com.example.finalhomework.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import kotlinx.coroutines.launch
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class PostsViewModel(): ViewModel() {


    val myResponse:MutableLiveData<Response<Posti?>?> = MutableLiveData()

    fun getPost(){
        viewModelScope.launch {
            val response:Response<Posti?>? = getPosts()
            myResponse.value = response
        }
    }

    suspend fun getPosts(): Response<Posti?>? {
        return RetrofitInstance.api.getPost()
    }




}