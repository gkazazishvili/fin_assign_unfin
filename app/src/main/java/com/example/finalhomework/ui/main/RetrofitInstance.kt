package com.example.finalhomework.ui.main

import com.example.finalhomework.ui.main.Constants.Companion.BASE_URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

object RetrofitInstance {

    private val retrofit =
        Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create())
            .build()


    val api:SimpleApi =
        retrofit.create(SimpleApi::class.java)
}

